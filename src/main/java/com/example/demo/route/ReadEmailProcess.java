package com.example.demo.route;


import com.google.api.client.util.StringUtils;
import com.google.api.services.gmail.model.MessagePart;
import com.google.api.services.gmail.model.MessagePartHeader;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ReadEmailProcess implements Processor {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());


    public void process(Exchange exchange) throws Exception {

        EmailBean emailBean = this.getValue(exchange.getMessage().getBody(com.google.api.services.gmail.model.Message.class)
                .getPayload());

        this.printEmail(emailBean);

    }

    private EmailBean getValue(MessagePart payloadMsgPart){

        String dateEmail = "";
        String from = "";
        String to = "";
        String subject = "";
        String emailBody = "";

        List<MessagePartHeader> headers = payloadMsgPart.getHeaders();
        //List<MessagePartHeader> headers = message.getPayload().getHeaders();

        if (!headers.isEmpty()) {
            for (MessagePartHeader header : headers) {
                String name = header.getName();
                switch (name) {
                    case "From":
                        from = header.getValue();
                        break;
                    case "To":
                        to = header.getValue();
                        break;
                    case "Subject":
                        subject = header.getValue();
                        break;
                    case "Date":
                        dateEmail = header.getValue();
//                        if(date.contains(","))
//                            date = date.substring(date.indexOf(",") + 2,date.length());;
//                        String timestampFormat = "dd MMM yyyy HH:mm:ss Z";
                        //dataEmail = TimeUtils.fromFormattedString(timestampFormat,date) / 1000;
                        break;
                }
            }
        }


        // Print email body
        if (payloadMsgPart.getParts() != null && !payloadMsgPart.getParts().isEmpty()) {
            emailBody = StringUtils
                    .newStringUtf8(Base64.decodeBase64(payloadMsgPart.getParts().get(0).getBody().getData()));
        }else{
            emailBody = StringUtils
                    .newStringUtf8(Base64.decodeBase64(payloadMsgPart.getBody().getData()));

        }

        EmailBean emailBean = new EmailBean(dateEmail, from, from, subject, emailBody);
        return emailBean;
}

    private void printEmail(EmailBean emailBean){
        log.debug("");
        log.debug("");
        log.debug(">>>>>>>>>>>>>>>>>>>> EMAIL - START <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
        log.debug("");

        log.debug(">>>> Date: " + emailBean.getDateEmail());
        log.debug(">>>> From: " + emailBean.getFrom());
        log.debug(">>>> To: " + emailBean.getTo());
        log.debug(">>>> Subject: " + emailBean.getSubject());
        log.debug(">>>> Body: " + emailBean.getEmailBody());

        log.debug("");
        log.debug(">>>>>>>>>>>>>>>>>>>> EMAIL - FINISH <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
        log.debug("");
        log.debug("");
    }


}
