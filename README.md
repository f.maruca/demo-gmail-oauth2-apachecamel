
### REQUISITI

	- Manen 3.6.3 o superiore
 
	- Java 11
	
	1) Su Google Cloud:
		1.1) Creare credenziali Google Cloud
		1.2) Scaricare il file delle credenziali .json

	2) Sul progetto:
		2.1) Rinominare il file delle credenziali (vedi punto 1) in "credentials.json";
		2.2) Metterlo nella directory "resource";
		2.3) Modificare il valore  della chiave gmail.archiver.email (in application.properties) con la mail di qui si vuole 
		    leggere le e-mail;



### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.